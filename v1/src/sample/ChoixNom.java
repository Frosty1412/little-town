package sample;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
public class ChoixNom extends FlowPane implements Initializable {
    @FXML
    public CheckBox check2;
    @FXML
    public CheckBox check3;
    @FXML
    public CheckBox check4;
    @FXML
    public Button valid;
    @FXML
    public Label lab1;
    @FXML
    public AnchorPane paneDepart;
    @FXML
    public Pane pane1;


    public  Label j1=new Label();
    public  Label j2=new Label();
    public  Label j3=new Label();
    public  Label j4=new Label();
    public TextField nomJ1=new TextField();
    public TextField nomJ2=new TextField();
    public TextField nomJ3=new TextField();
    public TextField nomJ4=new TextField();



    public void validNb() throws IOException {

        List<Label> list=new ArrayList<Label>();
        List<TextField> listText=new ArrayList<TextField>();
        listText=setText();
        if (valid.getText().equals("Commencer")){

            paneDepart.getChildren().remove(pane1);
            Pane fxmlLoader = FXMLLoader.load(getClass().getResource("Choose.fxml"));
            paneDepart.getChildren().setAll(fxmlLoader);
        }
        if (check2.isSelected()){
            list=setLabel(2);
            pane1.getChildren().removeAll(check2,check3,check4,lab1);

            for (int i=0;i<2;i++){
                list.get(i).setLayoutX(150*i+10);
                list.get(i).setLayoutY(50);
                listText.get(i).setLayoutX(150*i+10);
                listText.get(i).setLayoutY(100);

                pane1.getChildren().addAll(list.get(i));
                pane1.getChildren().addAll(listText.get(i));
                valid.setText("Commencer");

            }
        }else if(check3.isSelected()){
            list=setLabel(3);
            pane1.getChildren().removeAll(check2,check3,check4,lab1);
            for (int i=0;i<3;i++){
                list.get(i).setLayoutX(150*i+10);
                list.get(i).setLayoutY(50);
                listText.get(i).setLayoutX(150*i+10);
                listText.get(i).setLayoutY(100);

                pane1.getChildren().addAll(list.get(i));
                pane1.getChildren().addAll(listText.get(i));
                valid.setText("Commencer");
            }
        }else if(check4.isSelected()){
            list=setLabel(4);
            pane1.getChildren().removeAll(check2,check3,check4,lab1);
            for (int i=0;i<4;i++){
                list.get(i).setLayoutX(150*i+10);
                list.get(i).setLayoutY(50);
                listText.get(i).setLayoutX(150*i+10);
                listText.get(i).setLayoutY(100);

                pane1.getChildren().addAll(list.get(i));
                pane1.getChildren().addAll(listText.get(i));
                valid.setText("Commencer");
            }
        }


    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
    public List<Label> setLabel(int nb){
        List<Label> list=new ArrayList<Label>();
        list.add(j1);
        list.add(j2);
        list.add(j3);
        list.add(j4);
        for(int i=0 ;i<nb;i++){
            int truc=i+1;
            list.get(i).setText("Joueur "+truc+" entre son nom");
        }
        return list;
    }

    public List<TextField> setText(){
        List<TextField> list =new ArrayList<TextField>();
        list.add(nomJ1);
        list.add(nomJ2);
        list.add(nomJ3);
        list.add(nomJ4);
        return  list;

    }


}
