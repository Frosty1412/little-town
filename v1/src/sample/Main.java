package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        App root=new App();
        Scene scene=new Scene(root,1000,1000);
        primaryStage.setTitle("Little Town");
        primaryStage.setScene(scene);
        primaryStage.show();

    }


    public static void main(String[] args) throws NombreJoueursInvalide, IOException {
        launch(args);
    }

}

