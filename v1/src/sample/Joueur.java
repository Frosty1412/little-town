package sample;

import javafx.scene.control.Button;

import java.util.ArrayList;
import java.util.List;

public class Joueur {

    private String nom;

    private List<PionOuvrier> listeOuvrier;
    private List<Objectif> listeObjectifs;
    public List<Button> listeBatiment;
    private boolean isPremierJoueur;
    private String couleur;
    private int pionDepart;
    private int nbPiece1;
    private int ble=3;
    private int eau=3;
    private int pierre=3;
    private int bois=3;



    public Joueur(){
      listeBatiment = new ArrayList<>();
      listeOuvrier= new ArrayList<>();
      listeObjectifs= new ArrayList<>();

    }
    public Joueur(String nom){
        this.nom=nom;
        listeBatiment = new ArrayList<>();
        listeOuvrier= new ArrayList<>();
        listeObjectifs= new ArrayList<>();
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public boolean isPremierJoueur() {
        return isPremierJoueur;
    }



    public List<PionOuvrier> getListeOuvrier() {
        return listeOuvrier;
    }

    public List<Objectif> getListeObjectif(){
        return listeObjectifs;
    }

    public void setListeOuvrier(List<PionOuvrier> listeOuvrier) {
        this.listeOuvrier = listeOuvrier;
    }

    public void setPremierJoueur(boolean premierJoueur) {
        isPremierJoueur = premierJoueur;
    }

    public void addObjectif(Objectif o){
      listeObjectifs.add(o);
    }

    public int getPionDepart() {
        return pionDepart;
    }

    public void setPionDepart(int pionDepart) {
        this.pionDepart = pionDepart;
    }

    public String getCouleur() {
        return couleur;
    }

    public int getNbPiece1() {
        return nbPiece1;
    }

    public void setNbPiece1(int nbPiece1) {
        this.nbPiece1 = nbPiece1;
    }
    public void incNbPiece1(){
        this.nbPiece1++;
    }
    public void decNbPiece1(){
        this.nbPiece1--;
    }
    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public boolean choisirJoueur() {
        double rand = Math.random();
        if(rand < 0.5) {
            return true;
        } return false;
    }

    public int getBle() {
        return ble;
    }

    public void setBle(int ble) {
        this.ble = ble;
    }

    public void setBois(int bois) {
        this.bois = bois;
    }

    public int getBois() {
        return bois;
    }

    public void setEau(int eau) {
        this.eau = eau;
    }

    public void setPierre(int pierre) {
        this.pierre = pierre;
    }

    public int getEau() {
        return eau;
    }

    public int getPierre() {
        return pierre;
    }

}

